<?php

namespace App\Http\Controllers;

use App\Vehiculos;
use Illuminate\Http\Request;
use DB;
use App\Quotation;
class VehiculosController extends Controller
{
  public function showAllVehiculos()
      {
          $tbl_auto = DB::table('tbl_auto')->get();
          return response()->json($tbl_auto);
      }


      public function showVehiculo($id)
      {
        $auto = DB::select('select * from tbl_auto where id_auto = '.$id);
        return response()->json($auto);
      }

      public function showVehiculoNuc($nuc,$vin)
      {
        $auto = DB::select('select * from tbl_auto where carpeta = "'.$nuc.'" AND placas = "'.$vin.'"');
        return response()->json($auto);
      }
}
