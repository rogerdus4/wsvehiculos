<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vehiculos extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        ' id_auto,carpeta,placas, oficio_liberacion, permiso_provisional vin,modelo,n_motor, senias_particulares, id_autoridad, fecha_recuperacion, hora_recuperacion,id_entidad_recupera,id_municipio_recupera,id_color,id_clase_vehiculo,id_tipo_vehiculo,id_marca,id_submarca,id_entidad_entrega,id_municipio_entrega,fecha_entrega,hora_entrega ,nombre_entrega,paterno_entrega,materno_entrega,folio_c5 ,folio_fecha,liberado'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['created_at,updated_at'];
}
