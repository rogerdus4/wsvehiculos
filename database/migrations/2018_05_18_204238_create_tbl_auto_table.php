<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblAutoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_auto', function (Blueprint $table) {
            $table->increments('id_auto');
            $table->string('carpeta');
            $table->string('placas');
            $table->string('oficio_liberacion');
            $table->string('vin');
            $table->integer('modelo');
            $table->string('n_motor');
            $table->string('senias_particulares');
            $table->date('fecha_recuperacion');
            $table->integer('id_entidad_recupera');
            $table->integer('id_municipio_recupera');
            $table->integer('id_color');
            $table->integer('id_clase_vehiculo');
            $table->integer('id_marca');
            $table->integer('id_submarca');
            $table->integer('id_entidad_entrega');
            $table->integer('id_municipio_entrega');
            $table->date('fecha_entrega');
            $table->date('hora_entrega');
            $table->date('nombre_entrega');
            $table->date('paterno_entrega');
            $table->integer('folio_c5');
            $table->integer('folio_fecha');
            $table->integer('liberado');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_auto');
    }
}
